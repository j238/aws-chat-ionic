import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Auth } from 'aws-amplify';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router) { }

  canActivate() {
    console.log('AuthGuard#canActivate called');
    return Auth.currentAuthenticatedUser()
      .then(user => true)
      .catch(err => {
        this.router.navigate(['']);
        return false;
    });
   }
}
