import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Auth, Hub } from 'aws-amplify';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html'
})
export class NavComponent {
  isLoggedIn = false;

  constructor(public router: Router) {
    console.log('NavComponent: constructor');
    router.navigate(['/chat']);
    Hub.listen('auth', data => {
      console.log('Hub: auth:', data);
      const isLoggedIn = data.payload.event === 'signedIn' || data.payload.event === 'confirmSignIn';
      if (this.isLoggedIn && !isLoggedIn) {
        router.navigate(['']);
      } else if (!this.isLoggedIn && isLoggedIn) {
        router.navigate(['/chat']);
      }
      this.isLoggedIn = isLoggedIn;
    });
  }

  public signOut() {
    
    Auth.signOut();
  }
}
